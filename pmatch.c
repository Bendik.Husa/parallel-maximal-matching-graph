// Parallel greedy matching
//
// n     : number of vertices
// ver   : pointers into the edges array to specify where each edge list starts and ends
// edges : edge lists for each vertex
// p     : array of length (at least) n. This should contain the matching partner of each vertex
// s     : helper array of length n
// t     : helper array of length n
//

void match_set(int n, int *ver, int*edges, int *p, int *s, int*t) {
    #pragma omp for
    for(int i = 1; i <= n; i++) { 
        if (p[i] == 0) { 
            for(int j = ver[i]; j<ver[i+1]; j++) {
                if (p[edges[j]] == 0) { 
                    p[i] = edges[j]; 
                    p[edges[j]] = i;
                    break;           
                }
            }
        }
    }
} 

void match_set_single_naive(int n, int step, int *ver, int*edges, int *p, int *s, int*t) {
    #pragma omp master
    for(int i = 1; i <= n; i++) { 
        if (p[i] == 0) { 
            for(int j = ver[i]; j<ver[i+1]; j++) {
                if (p[edges[j]] == 0) { 
                    p[i] = edges[j]; 
                    p[edges[j]] = i;
                    break;           
                }
            }
        }
    }
} 

void match_set_single(int n, int step, int *ver, int *edges, int *p, int *s, int *t) {
    #pragma omp master
    for (int i = 0; i < n; i += step){  //for each n/p portion
      for (int j = 1; j <= t[i]; j++){  //loop through unmatched vertices
        if (p[s[i + j]] == 0) { //even though we know each vertex is initially unmatched, two of these may be matched during this loop
          for (int k = ver[s[i + j]]; k < ver[s[i + j] + 1]; k++){ //loop through their neighbor vertices
            if (p[edges[k]] == 0) { //if a neighbor is unmatched
              p[s[i + j]] = edges[k]; //match the vertex
              p[edges[k]] = s[i + j]; //and match the neighbor vertex
              break;           //stop looping through neighbors since a match is found
            }
          }
        }
      }
    }  
} 

void pmatch(int n, int *ver, int *edges, int *p, int *s, int *t) {
    int nr_matched;
    int nthreads = omp_get_num_threads();
    int tid = omp_get_thread_num();
    int j = 1;
    int step = (int)(n/nthreads); //defines the n/p portion for each thread
    
    #pragma omp for
    for(int i = 1; i <= n ; i++) p[i] = 0;
    
    for (int runs; runs < 1; runs++){

      match_set(n, ver, edges, p, s, t);
      #pragma omp barrier
      
      #pragma omp for
      for(int i = 1; i <= n; i++){
        if ((p[i] != 0) && (p[p[i]] != i)){ //if vertex is matched to a vertex that is matched to a different vertex
          p[i] = 0; //unmatch the vertex 
          s[tid*step + j] = i; //save the vertex in the n/p portion of s
          j++;
        }
      }

   
      t[tid*step] = j; //each thread stores their count of erroneouly matched vertices
      
      #pragma omp barrier

    }
      match_set_single(n, step, ver, edges, p, s, t);
} 
