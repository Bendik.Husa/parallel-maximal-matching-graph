CC = gcc
CFLAGS = -pg -O3 -std=gnu99 -m64
OMPFLAGS = -fopenmp -lnuma
LIBS = -lm


all: driver


driver: driver.c
	$(CC) $(CFLAGS) $(OMPFLAGS) driver.c -o $@ $(LIBS)

clean:
	rm -fr driver *.o

