// Sequential greedy matching
// --------------------------
// Traverses through the vertices and picks the first unmatched neighbor to match with.
// Two vertices i and j are matched if and only if p[i]=j and p[j]=i.
//
// Parameters:
// n     : number of vertices
// ver   : ver[i] points to the start of the neighbor list of vertex i in edges
// edges : lists of neighbors of each vertex
// p     : array used to define the matching
//
// Note that the vertices are numbered from 1 to n (inclusive). Thus there is
// no vertex 0.

void smatch(int n ,int *ver, int *edges, int *p) {

  int i, j;

  for(i = 1; i <= n ; i++)   // Set that every node is unmatched
    p[i] = 0;

  for(i = 1; i <= n; i++) {                // Loop over vertices
    if (p[i] == 0) {                 // Check if this vertex has been matched
      for(j = ver[i]; j<ver[i+1]; j++) { // Loop over neighbors of vertex i
        if (p[edges[j]] == 0) {      // Check if neighbor vertex is unmatched
          p[i] = edges[j];           // Match i and edges[j]
          p[edges[j]] = i;
//          nr_matched += 2;
          break;                     // Continue to next vertex
        }
      }
    }
  } // loop over vertices
  
}
